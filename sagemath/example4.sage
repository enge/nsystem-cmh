"""
#*******************************************************************************
# Copyright (C) 2011 - 2024 Marco Streng
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/
#*******************************************************************************


This file contains the computations for the example
in Section 7.5 of "Schertz style class invariants
for higher degree CM fields" by Enge and Streng.

It was tested with SageMath 10.2 and Recip 3.4.1, available at
https://www.sagemath.org
https://github.com/mstreng/recip
https://bitbucket.org/mstreng/recip/

To test this file automatically, use "sage -t file_location/example4.sage"
while in the home directory of Recip.
All computations in this file together take 2 minutes in a single thread on
a laptop with a 3,3 GHz Dual-Core Intel Core i7.

First load Recip, e.g. by changing to the main directory of Recip
and then doing this::

    sage: load("recip.sage")

We use the CM field [5, 11, 29], that is x^4 + 11*x^2 + 29 = 0
with K0 = QQ(sqrt(5)), and we use N = 14::
                                                 
    sage: K0.<o> = NumberField(x^2-x-1)
    sage: P.<X> = PolynomialRing(K0)
    sage: p = (X^4+11*X^2+29).factor()[0][0]
    sage: p
    X^2 - o + 6

So we could identify o with x^2+6. (And we do below.) It is omega in the paper.

We use the following N-system (at the bottom of the file we show how we found it)::

    sage: [A, B, C] = [1, 12*o - 2, 28*o + 42]

We create the CM field::

    sage: K = CM_Field([5, 11, 29], name='x'); x = K.gen()
    sage: P2 = K.ideal(2, x^2 + x + 1)
    sage: P2^2 == K.ideal(2)
    True
    sage: P2.is_principal()
    False
    sage: P7 = K.ideal(x^2 + x + 6)
    sage: P7bar = K.ideal(x^2 - x + 6)
    sage: P7*P7bar == K.ideal(7)
    True
    sage: [phi(o) for phi in K0.embeddings(K)]
    [x^2 + 6, -x^2 - 5]

So indeed o = x^2 + 5 or -x^2-6. We take the first one::

    sage: [emb] = [phi for phi in K0.embeddings(K) if phi(o) == x^2 + 6]
    sage: Q.<Y> = K[]
    sage: z = (Y^2+emb(B)*Y+emb(C)).roots()[0][0]
    sage: K.ideal(A*z, 14) == P2*P7
    True
    sage: om = emb(o)     
    sage: l = (2*om-1) # lambda in the paper can be l or -l
    sage: l^2 == 5
    True  
    sage: om
    x^2 + 6
    sage: bas = [z*om, z, K(-1), 1-om]  # the basis from the paper
    sage: xi = ((z-K.complex_conjugation()(z))*l)^-1
    sage: (2*x^3+16*x)*xi == 1
    True
    sage: [Phi] = [Phi for Phi in K.CM_types() if all([CC(phi(xi)).imag() > 0 for phi in Phi])]
    sage: Z = PeriodMatrix_CM(CM_type=Phi, xi=xi, basis=bas)

Recip does not currently allow multiplication of period matrices by
rational numbers. Hence we do it like this:
  
    sage: def matrix_div(Z, N):
    ....:     basZ = Z.basis()
    ....:     g = len(basZ)/2
    ....:     bas = [b/N for b in basZ[:g]] + basZ[g:]
    ....:     return PeriodMatrix_CM(CM_type=Z.CM_type(), xi=N*Z.xi(), basis=bas)
    sage: Z2 = matrix_div(Z, 2)
    sage: Z7 = matrix_div(Z, 7)
    sage: Z14 = matrix_div(Z, 14)

And a sanity check::

    sage: Z14 == Z.Sp_action(Matrix([[1/14,0,0,0],[0,1/14,0,0],[0,0,1,0],[0,0,0,1]]))
    True
    sage: str(Z14) == str(Z.Sp_action(Matrix([[1,0,0,0],[0,1,0,0],[0,0,14,0],[0,0,0,14]])))
    True

Igusa invariants::

    sage: i = igusa_modular_forms() # h4, h6, h10, h12
    sage: h4 = i[0]
    

At the end of Section 7.5 we explain why we can't use
double Igusa quotients. In fact, we can check that in this
case the double h4-quotients seems to be equal to 1::

    sage: [h4(Z2,prec=prec)*h4(Z7,prec=prec)/h4(Z,prec=prec)/h4(Z14,prec=prec) for prec in [50, 100, 150]]
    [1.0000007608257 + 3.2838323248408e-6*I,
     0.99999999982768442465340382685 - 4.9632578373479045250694601846e-10*I,
     1.0000000000041704324517569339079904636849552 - 5.8014357256145733565454011693358661238526624e-13*I]
 
Solution: don't use double eta quotient, but use e.g. 
f = h + h*Fricke     for a Gamma^0(N)-function       h.

For h we use the simple Igusa quotient h4(Z/14) / h4(Z).

Then::

    sage: def Fricke(tau, N):
    ....:     return tau.Sp_action(Matrix([[0,0,N,0],[0,0,0,N],[-1,0,0,0],[0,-1,0,0]]))
    sage: prec=800
    sage: f = h4(Z14,prec=prec,interval=True) / h4(Z,prec=prec,interval=True) + h4(matrix_div(Fricke(Z,14),14),prec=prec,interval=True) / h4(Fricke(Z, 14),prec=prec,interval=True)
    sage: f
    -65577.5? + 546773.0?*I
    sage: (f - (-655775/10+5467730/10*f.parent().gen())).abs().upper()
    0.07...
    
This is non-real. Moreover::

    sage: icp = class_polynomials(K); icp # proven, using interval arithmetic
    [x^4 - 5807347581960/529*x^3 + 5119233269154918689016/529*x^2 - 21934383465976000818372000/529*x + 22311935904730227232412250000/529,
     366565465737583286508/671898241*x^3 - 405240011492974060089161336040/671898241*x^2 + 230108041570014775225278587790000/95985463*x - 3914724040446717670033517242500000/1958887,
     272385432763672029738013125000/671898241*x^3 - 286086328055210485147038247177161750000/671898241*x^2 + 175113611645005201925712142416305812500000/95985463*x - 3635262983506436101469985555159984375000000/1958887]
    sage: Kr = Z.CM_type().reflex_field()
    sage: icp[0].roots(Kr)
    [(464765202*xr^2 + 10118094264, 1),
     (23738340978/529*xr^2 + 516792386520/529, 1),
     (-23738340978/529*xr^2 - 5451114996/529, 1),
     (-464765202*xr^2 - 106740180, 1)]

Conclusion: the CM class field is Kr, and hence f(Z) lies in Kr,
hence has linear minimal polynomial over Kr. But since f(Z) is non-real,
the coefficients do not lie in Kr0. In particular, the conclusion
of Theorem 5.4 does not hold. We now check that all hypotheses
except (5.4) do hold.

First of all, f is the quotient of two modular forms with rational
q-expansions and is invariant under Gamma^0(N). This is true
because it is true for h = i4(Z/14)/i4(Z) and preserved by
the Fricke involution. Alternatively, it is true because
it is true for h and we see below that f = h + 14^8/h.
The field K0 is totally real, and D_{K_0} is generated by l.
We have already checked N | C, and a generator z of K is obtained
as a root of AX^2+BX+C, hence the discriminant is totally negative.
tau = Z is constructed using the formulas of Corollary 3.3, which
is a special case of 3.1 and 3.2. The N-system conditions are satisfied
for this Z. We also have::

    sage: (C/14).norm()
    11

Coprime to 14. And f was specifically constructed to be invariant
under the Fricke involution. So indeed all conditions except (5.4)
are satisfied. This proves that (5.4) is necessary.

Here is another example we considered (with the same K and Z)::

    sage: h4(Z,prec=prec,interval=True) / h4(Z14,prec=prec,interval=True) + h4(Fricke(Z,14),prec=prec,interval=True) / h4(matrix_div(Fricke(Z, 14),14),prec=prec,interval=True)
    -0.000044435554? + 0.000370495340?*I

Again non-real, again proving that (5.4) is necessary.
    
Here is an alternative way of computing f:

With h = i(Z/N)/i(Z) with i of level 1 and weight k
we get h*Fricke = h(-N/Z) = i(-1/Z)/i(-N/Z)
                = det(-Z)^k i(Z) / det(-Z/N)^k i(Z/N) = N^(kg) / h

With h = i(Z)/i(Z/N) with i of level 1 and weight k
we would have gotten h*Fricke = h(-N/Z) = i(-N/Z)/i(-1/Z)
                              = det(-(Z/N))^k i(Z/N) / det(-Z)^k i(Z)
                              = 1 / (N^(kg) h)

We double-check some things::

    sage: prec = 1200
    sage: h = h4(Z14,prec=prec) / h4(Z,prec=prec) # Simple I4 quotient
    sage: poly = algdep(h, 4, known_bits=250)
    sage: poly[4].factor()
    7^2
    sage: poly[0].factor()
    2^16 * 3^8 * 7^14

That's highly unlikely to come from chance.::

    sage: rts = poly.roots(Kr)
    sage: len(rts)
    2

That's h and hbar, again highly unlikely to come from chance::

    sage: rts = [(abs(Kr.embedding()(r[0])-h), r[0]) for r in rts]
    sage: rts.sort()
    sage: r = rts[0][1]
    sage: abs(Kr.embedding()(r) - h)
    1.1...e-78

Really tiny, so this is the correct one.

Let's check that h+14^8/h is indeed f::

    sage: f2 = Kr.embedding()(r+14^8/r); f2
    -65577.503994117? + 546772.96803622?*I
    sage: f
    -65577.5? + 546773.0?*I

And the fact that this agrees does not come from one of the two terms dominating::

    sage: Kr.embedding()(r)
    -314.625527678? - 2648.70455793?*I
    sage: 14^8/Kr.embedding()(r)
    -65262.878467? + 549421.672594?*I

Let's quadruple-check our numerical computation of f
(we already double-checked it with r)::

    sage: f3 = h + 14^8/h # This is like f2, but computed in a different order
    sage: Zn = Z.complex_matrix(1200)
    sage: f4 = h + h4((-14*Zn^-1)/14) / h4(-14*Zn^-1) # The most direct
    sage: (f3-f4).abs()
    6.7...e-22
    sage: (f-f2).abs()
    0.1?
    sage: (f2.n(1200)-f3).abs()
    2.3956...e-76
    
So they all agree very much. Only f and f4 suffer from slow
convergence, but f4 agrees too much with f3 for the values to be
a coincidence.

Next, the minimal polynomial::

    sage: fpoly = (r+14^8/r).minpoly()
    sage: d = lcm([c.denominator() for c in fpoly])
    sage: fpoly = d*fpoly
    sage: fpoly(f).abs() # f is not precise enough to say anything:
    1.?e23
    sage: fpoly(f2).abs() # neither is f2:
    1.?e16
    sage: fpoly(f3).abs() # this looks good:
    7.9...e-53
    sage: fpoly(f4).abs() # and not too bad:
    223.5...
    sage: fpoly[4].factor()
    3^8 * 7^2
    sage: fpoly
    321489*x^4 + 576756153216*x^3 + 437154939964495872*x^2 + 197472971374499524902912*x + 81742666063419219038606196736

And an expression of f in terms of a small generator of Kr::

    sage: Kr
    CM Number Field in xr with defining polynomial x^4 + 22*x^2 + 5
    sage: y = Kr.gen()
    sage: (y^2+11)^2
    116
    sage: Kr.embedding()(y^2+11)
    -10.7703296142690?
    sage: Kr.embedding()(y)
    4.66586858090421?*I
    sage: 2^5 * 3^-2* 7^-2 *(-587080*y^3 - 489976*y^2 - 11165960*y - 11570679) - (r+14^8/r)
    0
    
So f = 2^5 * 3^-2* 7^-2 *(587080*y^3 - 489976*y^2 + 11165960*y - 11570679)
where y^2 + 11 = -sqrt(116) and y has positive imaginary part, hence
y = i*sqrt(11+sqrt(116)).

Finally, we get back to how we found the N-system.
In this example, we used a by-hand and ad hoc approach to finding
an N-system (as apposed to the methods explained in the paper).

We start with the polynomial X^2 + 4*o + 5 of which x*o is a root
(we could have also started with X^2 - o + 6 of which x is a root),
and look for a polynomial with N | C, where N = P7*P2 as follows::

    sage: p = X^2 + 4*o + 5
    sage: P7 = K0.ideal(7) 
    sage: P7.is_prime()
    True
    sage: k7 = P7.residue_field()
    sage: k7.lift(k7(-p[0]).sqrt())   
    6*o + 6
    sage: P2 = K0.ideal(2) 
    sage: P2.is_prime()
    True
    sage: k2 = P2.residue_field()
    sage: k2.lift(k2(-p[0]).sqrt())   
    1

So we translate by something that is 6*o + 6 mod 7 and 1 mod 2,
such as 6*o - 1::

    sage: q = p(X+6*o-1)
    sage: q
    X^2 + (12*o - 2)*X + 28*o + 42
    sage: A == q[2] and B == q[1] and C == q[0]
    True

So these are exactly the A, B, C listed in the paper.

"""