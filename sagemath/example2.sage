"""
#*******************************************************************************
# Copyright (C) 2011 - 2024 Marco Streng
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/
#*******************************************************************************


This file contains a SageMath verification of the example
in Section 7.3 of "Schertz style class invariants
for higher degree CM fields" by Enge and Streng.
For a more structured computation, see example4.sage (Section 7.5)
or the GP files (Section 7.3).

It was tested with SageMath 10.2 and Recip 3.4.1, available at
https://www.sagemath.org
https://github.com/mstreng/recip
https://bitbucket.org/mstreng/recip/

First update the location of Recip in scripts.sage and then do this::

    sage: load("scripts.sage")

Alternatively, if your SageMath installation is built with SSL, remove
the lines with load_attach_path and load("recip.sage") and do this::

    sage: load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage") # optional: ssl
    sage: load("scripts.sage") # optional: ssl

To test this file automatically, use "sage -t example2.sage" or
"sage -t --long example2.sage".
All computations in this file together take 3 minutes (7 minutes with "long")
in a single thread on a laptop with a 3,3 GHz Dual-Core Intel Core i7.

We start with the 2-system and a verification of some expressions
for (elements of) K and Kr0 from the paper::

    sage: K0.<sqrt13> = QuadraticField(13)
    sage: K = CM_Field([13, 18, 68])
    sage: x = K.gen()
    sage: omega = (1+sqrt13)/2
    sage: ABC_in_K0 = [[1, 0, 16*omega + 22], [5*omega+7, 8*omega + 8, 2*omega + 8], [5*omega + 7, -8*omega - 8, 2*omega + 8], [-omega + 5, 0, 6*omega + 8]]
    sage: N = 2
    sage: [A1, B1, C1] = ABC_in_K0[0]
    sage: D = B1^2 - 4*A1*C1
    sage: [K0_to_K] = [phi for phi in K0.embeddings(K) if phi(D).is_square()]
    sage: assert K0_to_K(omega) == (x^2+10)/2
    sage: assert K0_to_K(sqrt13) == x^2 + 9
    sage: assert all([C in K0.ideal(N) for [A,B,C] in ABC_in_K0])
    sage: assert all([(B - B1) in K0.ideal(2*N) for [A,B,C] in ABC_in_K0])
    sage: assert all([K0.ideal(N, C/N) == 1 for [A,B,C] in ABC_in_K0])
    sage: assert all([K0.ideal(N, A) == 1 for [A,B,C] in ABC_in_K0])
    sage: ABC = [[K0_to_K(a) for a in b] for b in ABC_in_K0]
    sage: z = [(-B + sqrt(B^2-4*A*C))/2/A for [A,B,C] in ABC]
    sage: w = K0_to_K(omega)
    sage: tau = [period_matrix_from_z(zi, bas=[1, w], lmb=K0_to_K(sqrt13)) for zi in z]
    sage: P.<y> = QQ[]
    sage: Kr0.<omegar> = NumberField((y-1/2)^2-17/4)
    sage: assert Kr0.discriminant() == K.Phi().reflex_field().DAB()[0]

We evaluate f in the N-system::

    sage: prec = 1000
    sage: #[phi] = [phi for phi in Kr0.embeddings(RealField(prec)) if phi(omegar)>0]
    sage: sqrtf = [AIx(period_matrix_div(t, 2), prec=prec) / AIysqrt(period_matrix_div(t, 2), prec=prec) for t in tau]
    sage: f = [a^2 for a in sqrtf]
    sage: X = polygen(ComplexField(prec))
    sage: polyf = prod([X-a for a in f])

We recognize the class polynomial numerically (heuristically) as a polynomial
over Kr0 and confirm that this is the polynomial from the paper::

    sage: lower_prec = 200
    sage: polyf_real = RealField(lower_prec)['x']([c.real() for c in list(polyf)])
    sage: [phi] = [phi for phi in Kr0.embeddings(RealField(lower_prec)) if phi(omegar)>0]
    sage: poly = recognize_polynomial(polyf_real, emb=phi)
    sage: poly.denominator().factor()
    19^4 * 59^2
    sage: poly*poly.denominator()
    453647401*y^4 + (41960216624328*omegar - 116332595812008)*y^3 + (-924565238142480*omegar + 2383794199841616)*y^2 + (8404908240715776*omegar - 21543961272975360)*y - 10331028745814016*omegar + 26471539326320640
    sage: X = polygen(Kr0, 'y')
    sage: polyf_paper = 19^4 * 59^2 * X^4 + (41960216624328 * omegar - 116332595812008) * X^3 + (-924565238142480 * omegar + 2383794199841616) * X^2 + (8404908240715776 * omegar - 21543961272975360) * X + (-10331028745814016 * omegar + 26471539326320640) # copied from the tex file
    sage: poly*19^4*59^2 == polyf_paper
    True

Here is the Igusa class polynomial as it appears in the paper (obtained using CMH)::

    sage: X = polygen(Kr0)
    sage: polyj_paper = 19^2 * 59^2 * X^4 + (1381745663216332313130 * omegar - 3547293859211493542130) * X^3 + (148473995403415029782069841975 * omegar - 380321923961391822525781469475) * X^2 + (5344671730358474048907677495421000 * omegar - 13690639163949002342762017668129000) * X + (52888480565700710835194263641602550000 * omegar - 135476567266153427225864462713788270000)

Here is the slow, but proven, alternative way of computing Igusa class polynomials::

    sage: # long time (6 minutes)
    sage: icp = class_polynomials(K) # slow, but proven, alternative way of computing Igusa class polynomials
    sage: polyj_paper in [p*p.denominator() for (p,e) in Kr0['x'](icp[0]).factor()]
    True

Theoretically, the Igusa class polynomial over Kr0 could have still been the other 3 factors::

    sage: # long time (uses the polynomial from before)
    sage: len(Kr0['x'](icp[0]).factor())
    4
    sage: prec=200
    sage: [phi] = [phi for phi in Kr0.embeddings(RealIntervalField(prec)) if phi(Kr0.gen()) > 0]
    sage: P.<x> = PolynomialRing(RealIntervalField(prec))
    sage: I4 = igusa_modular_forms()[0]
    sage: I6 = igusa_modular_forms()[1]
    sage: I10 = igusa_modular_forms()[2]
    sage: j = [I4(t, prec=prec, interval=True)*I6(t, prec=prec, interval=True)/I10(t, prec=prec, interval=True) for t in tau]
    sage: for (p,e) in Kr0['x'](icp[0]).factor():
    ....:     print(p*p.denominator() == polyj_paper, [0 in P(p)(t) for t in j])
    False [False, False, False, False]
    True [True, True, True, True]
    False [False, False, False, False]
    False [False, False, False, False]

Each "False" in the list excludes the corresponding polynomial.
This proves the correctness of polyj_paper.

How about the minimal polynomial of sqrt(f(tau))?::

    sage: d = 19^2*59
    sage: p = poly(y/d^2)*d^8
    sage: L.<b> = NumberField(p)
    sage: (b/d^2).sqrt().minpoly()*d
    21299*x^4 + (-2523732*omegar + 9426660)*x^3 + (17576244*omegar - 46804644)*x^2 + (-15869952*omegar + 38596608)*x - 49372416*omegar + 129309696
    

"""



