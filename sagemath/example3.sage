"""
#*******************************************************************************
# Copyright (C) 2011 - 2024 Marco Streng
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/
#*******************************************************************************


This file contains a verification of the example
in Section 7.4 of "Schertz style class invariants
for higher degree CM fields" by Enge and Streng.

It was tested with SageMath 10.2 and Recip 3.4.1, available at
https://www.sagemath.org
https://github.com/mstreng/recip
https://bitbucket.org/mstreng/recip/

To test this file automatically, use "sage -t file_location/example3.sage"
while in the home directory of Recip.
All computations in this file together take 3 minutes in a single thread on
a laptop with a 3,3 GHz Dual-Core Intel Core i7.

First load Recip, e.g. by changing to the main directory of Recip
and then doing this::

    sage: load("recip.sage")

We use the CM field [5, 53, 601] with N = 6::

    sage: P.<X> = QQ[]
    sage: K = CM_Field(X^4+53*X^2+601, name='x'); x = K.gen()
    sage: omega = (x^2+31)/9
    sage: K0.<o> = NumberField(omega.minpoly())
    sage: emb = K0.hom([omega])
    sage: assert K0.discriminant() == 5
    sage: icp = class_polynomials(K)[0]; icp
    x^10 - 898189527574479/43264*x^9 + 190932329857084956221809709947/1871773696*x^8 + 23796698093847665890434815868487701/9214885888*x^7 + 3432471889345590869126193575639634925692661/122668560941056*x^6 + 5080821341938731034624313532435929930911602039851/31403151600910336*x^5 + 4074022234802223046544005949464348020764384173745625/7850787900227584*x^4 + 963292653982830700089535727594983807105982371132734375/981348487528448*x^3 + 327672898692795610671524585506968263877964257545478515625/245337121882112*x^2 + 101003735243196296829090262361562445917768201309876708984375/122668560941056*x + 43432219495233533313068267909999372990411660164062286376953125/30667140235264
    sage: Kr0.<omegar> = NumberField((X-1/2)^2-601/4)
    sage: Q.<Y> = Kr0[]
    sage: F = [2^40*13^4*p for (p,e) in Q(icp).factor()]; F # We'll see later that the first one is correct
    [31403151600910336*Y^5 + (-6140585422220204445794304*omegar - 322904904921695447307780096)*Y^4 + (-96632884032276403274175741952*omegar - 4131427744203466842763320885248)*Y^3 + (-961856435411091691207536138780672*omegar - 19922426752533168631849612073238528)*Y^2 + (-2810878875032206947279703590350876416*omegar - 32507451628887950858017880191429021184)*Y - 3949991728992949515358757855080152530801*omegar - 59187968308773159157484805661633506074674,
     31403151600910336*Y^5 + (6140585422220204445794304*omegar - 329045490343915651753574400)*Y^4 + (96632884032276403274175741952*omegar - 4228060628235743246037496627200)*Y^3 + (961856435411091691207536138780672*omegar - 20884283187944260323057148212019200)*Y^2 + (2810878875032206947279703590350876416*omegar - 35318330503920157805297583781779897600)*Y + 3949991728992949515358757855080152530801*omegar - 63137960037766108672843563516713658605475]
    
So we identify o with omega = (x^2+31)/9.

We use the following N-system (see the paper or the GP scripts for how to find it)::

    sage: N = 6
    sage: N_syst = [[1, o+5, 6*o+12],
    ....:           [-3*o+7,145*o-211,-1110*o+1866],
    ....:           [3*o+7,-143*o+5,294*o+606],
    ....:           [-34*o+157, -3959*o+2309, 4194*o+34356],
    ....:           [o+2, -71*o+5, 180*o+546]]
    sage: OK0 = K0.maximal_order()
    sage: for [A,B,C] in N_syst:
    ....:     assert (C/N) in OK0
    ....:     assert K0.ideal(C/N,N) == 1
    ....:     assert K0.ideal(A,N) == 1
    ....:     assert (B-N_syst[0][1])/N in OK0

Now we create z = z_1 and the corresponding period matrix::

    sage: [A,B,C] = N_syst[0]
    sage: Q.<Y> = K[]
    sage: z = (Y^2+emb(B)*Y+emb(C)).roots()[0][0]
    sage: om = omega
    sage: bas = [z*om, z, K(-1), 1-om]  # the basis from the paper
    sage: l = K(5).sqrt()
    sage: l
    2/9*x^2 + 53/9
    sage: xi = ((z-K.complex_conjugation()(z))*l)^-1
    sage: [Phi] = [Phi for Phi in K.CM_types() if all([CC(phi(xi)).imag() > 0 for phi in Phi])]
    sage: Z = PeriodMatrix_CM(CM_type=Phi, xi=xi, basis=bas)

Recip does not currently allow multiplication of period matrices by
rational numbers. Hence we do it like this:
  
    sage: def matrix_div(Z, N):
    ....:     basZ = Z.basis()
    ....:     g = len(basZ)/2
    ....:     bas = [b/N for b in basZ[:g]] + basZ[g:]
    ....:     return PeriodMatrix_CM(CM_type=Z.CM_type(), xi=N*Z.xi(), basis=bas)

And a sanity check::

    sage: matrix_div(Z, 6) == Z.Sp_action(Matrix([[1/6,0,0,0],[0,1/6,0,0],[0,0,1,0],[0,0,0,1]]))
    True
    sage: str(matrix_div(Z, 6)) == str(Z.Sp_action(Matrix([[1,0,0,0],[0,1,0,0],[0,0,6,0],[0,0,0,6]])))
    True

Igusa invariants::

    sage: i = igusa_modular_forms() # h4, h6, h10, h12
    sage: [h4, h6, h10, h12] = i

Let's check that the first Igusa class polynomial factor is the correct one::

    sage: prec=200
    sage: t = h4(Z, prec=prec, interval=True)*h6(Z, prec=prec, interval=True) / h10(Z, prec=prec, interval=True)
    sage: [p(t) for p in F]
    [0.?e12 + 0.?e11*I,
     4.012288353022501732464429790822171292155854816476297098?e66 + 0.?e12*I]
 
Now the values we are interested in. First we compute all z and Z from
the N-system in a direct way (which is not going to work)::

    sage: Zs = []
    sage: for [A,B,C] in N_syst:
    ....:     z_candidates = [z for (z,e) in (emb(A)*Y^2 + emb(B)*Y + emb(C)).roots()]
    ....:     z_candidates = [(z, ((z-K.complex_conjugation()(z))*l)^-1) for z in z_candidates]
    ....:     [(z, xi)] = [t for t in z_candidates if all([CC(phi(t[1])).imag() > 0 for phi in Phi])]
    ....:     bas = [z*om, z, K(-1), 1-om]
    ....:     Zs.append(PeriodMatrix_CM(CM_type=Phi, xi=xi, basis=bas))
    sage: interval = True
    sage: fs = [h10(matrix_div(Z, 2), prec=prec, interval=interval) * h10(matrix_div(Z, 3), prec=prec, interval=interval) / h10(Z, prec=prec, interval=interval) / h10(matrix_div(Z, 6), prec=prec, interval=interval) for Z in Zs]
    sage: fs
    [137.649141098334180? + 1764.042258915256364?*I,
     [.. NaN ..] + [.. NaN ..]*I,
     -0.0002? - 0.0004?*I,
     [.. NaN ..] + [.. NaN ..]*I,
     -0.000214494164322? + 0.000339603875651?*I]

The problem is that we are far outside the fundamental domain. So
we use the following function adapted from scripts.sage::

    sage: def evaluate_mod_form(mf, Z, prec, interval=False):
    ....:     (U, M) = Z.reduce(200, transformation=True)
    ....:     A,B,C,D = ABCD(M)
    ....:     k = mf.weight()
    ....:     return (C * Z.complex_matrix() + D).determinant()^-k * mf(U, prec=prec, interval=interval)
    sage: fs = [evaluate_mod_form(h10, matrix_div(Z, 2), prec=prec, interval=interval) * evaluate_mod_form(h10, matrix_div(Z, 3), prec=prec, interval=interval) / evaluate_mod_form(h10, Z, prec=prec, interval=interval) / evaluate_mod_form(h10, matrix_div(Z, 6), prec=prec, interval=interval) for Z in Zs]
    sage: fs
    [137.649141098334180086518530166493440860739952579117342? + 1764.042258915256363418671000133556378777373534596785453?*I,
     1.97973751441596714784585401196792627811350466036768740? + 0.?e-53*I,
     -0.000214494164321577325237960013923369087883062455911748673? - 0.000339603875650956485014453932762817807993082482943792330?*I,
     137.6491410983341800865185301664934408607399525791174? - 1764.0422589152563634186710001335563787773735345967855?*I,
     -0.000214494164321577325237960013923369087883062455911748673? + 0.000339603875650956485014453932762817807993082482943792330?*I]
    sage: C = fs[0].parent()
    sage: P.<X> = C[]
    sage: polyC = prod([X-f for f in fs])
    sage: polyC
    X^5 + (-277.2775907227556841662324384249269612614176436936906? + 0.?e-49*I)*X^4 + (3.1313372766719955915517752673260698505794411542132644?e6 + 0.?e-46*I)*X^3 + (-6.196803812005553418028096277388080170018959987726150?e6 + 0.?e-45*I)*X^2 + (-2658.427567931200512390287288267359395208596619489356? + 0.?e-48*I)*X - 1.0000000000000000000000000000000000000000000000000000? + 0.?e-52*I
    sage: D = ComplexField(150)
    sage: Q.<y> = D[]
    sage: polyD = Q(polyC)
    sage: [embKr0] = [phi for phi in Kr0.embeddings(D) if phi(2*Kr0.gen()-1) > 0]
    sage: poly = recognize_polynomial(polyD, emb=embKr0)
    sage: 2^4*13^4*poly
    456976*y^5 + (-53182948*omegar + 551780268)*y^4 + (22828729975*omegar + 1139705021035)*y^3 + (-46035175179*omegar - 2244489935231)*y^2 + (10035944*omegar - 1342872664)*y - 456976

Finally, note that h_{10} is the square of a modular form Theta, hence
f is the square of a modular function. It therefore is not unlikely that
f(tau) is (possibly up to a small rational number depending only on f)
a square in K(f(tau)). Here are three ways of checking this, where the first
two give a speedup of the calculation of K(f(tau)).

First of all, if f is a modular function for Gamma0(M) for some M, then we can simply
change our N-system into an M-system and use evaluate_Theta from scripts.sage
instead of evaluating h_10.

Second of all, we can multiply N=6 by 8 to get an M for which f is a modular
function for Gamma(M). The explicit reciprocity law applies. To use it
here we would need to extend recip in order to be able to compute Galois
orbits when expressions involve things like h(Z/2) and h(Z/3).

Third (but algorithmically not efficient), since we have already computed
the minimal polynomial of f(tau), we can now compute the minimal polynomials
of its square roots::

    sage: y = poly.variables()[0]
    sage: f = poly(y/676)*676^5
    sage: y = poly.variables()[0]
    sage: c = 26
    sage: f = poly(y/c^2)*c^10
    sage: L.<b> = NumberField(f)
    sage: b.is_square()
    True
    sage: poly_root = (b.sqrt()/c).minpoly()*c^2; poly_root
    676*x^5 + (1326*omegar + 23894)*x^4 + (8833*omegar + 1025477)*x^3 + (-14003*omegar - 1482307)*x^2 + (-2040*omegar - 6080)*x - 676

    


"""