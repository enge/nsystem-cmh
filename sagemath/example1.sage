"""
#*******************************************************************************
# Copyright (C) 2011 - 2024 Marco Streng
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/
#*******************************************************************************


This file contains a SageMath computation of the example
in Section 7.2 of "Schertz style class invariants
for higher degree CM fields" by Enge and Streng.
For a more structured computation, see example4.sage (Section 7.5)
or the GP files (Section 7.2).

It was tested with SageMath 10.2 and Recip 3.4.1, available at
https://www.sagemath.org
https://github.com/mstreng/recip
https://bitbucket.org/mstreng/recip/

First update the location of Recip in scripts.sage and then do this::

    sage: load("scripts.sage")

Alternatively, if your SageMath installation is built with SSL, remove
the lines with load_attach_path and load("recip.sage") and do this::

    sage: load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage") # optional: ssl
    sage: load("scripts.sage") # optional: ssl

To test this file automatically, use "sage -t example1.sage".
All computations in this file together take about a minute in a single thread on
a laptop with a 3,3 GHz Dual-Core Intel Core i7.

We start with the standard CM inputs and give some names to certain elements
of K and K^r. This agrees with what is in the article::

    sage: K = CM_Field([5,57,661])
    sage: x = K.gen()
    sage: y = x^2
    sage: omega = (y+34)/11
    sage: assert omega^2-omega-1 == 0
    sage: lmb = 2*omega-1
    sage: assert lmb^2 == 5
    sage: [Phi] = [Phi for Phi in K.CM_types() if all([imag(CC(phi(x))) > 0 for phi in Phi.embeddings()])]
    sage: 57^2 > 11^2*5
    True
    sage: l = Phi.embeddings()
    sage: l.sort()
    sage: [phi1,phi2] = l
    sage: assert phi1(lmb) > 0 and phi2(lmb) < 0
    sage: icp = class_polynomials(K) # a minute or 2
    sage: P.<x> = QQ[]
    sage: K0r.<omegar> = NumberField(x^2-x-165)
    sage: x = K.gen()
    sage: K0r['x'](icp[0]).factor()[0][0]*841
    841*x^3 + (-5611098752*omegar - 17741044214880)*x^2 + (3232391784287232*omegar - 68899837678801920)*x + 7331944332391841792*omegar - 131969791422849515520

This proves that the polynomial printed in the article is indeed an
Igusa class polynomial of one of the CM types of K. We still need
to check that we have the correct one. We do that close to the end
of this file.

First, let's check the statements about the factorisation
of (3) and let's take an N-system.
Actually, we will use a different N-system than the one in the article.
If at least one element of the N-system agrees with the paper, then
the output does not depend on the choices made in creating
the rest of the N-system. We use the same A1, B1, C1 as in the paper,
but much simpler A2, B2, C2 and A3, B3, C3 computed by hand instead of
with the function nsystem in CMH::

    sage: K.ideal(3).factor()
    (Fractional ideal (3, 1/22*alpha^2 - 1/2*alpha + 23/22)) * (Fractional ideal (3, -1/22*alpha^2 - 1/2*alpha - 23/22))
    sage: K.real_field().ideal(3).factor()
    Fractional ideal (3)
    sage: ABC = [[1, 1, 3*omega+6], [lmb*omega, 1, 3], [lmb*omega, 19, -18*omega+57]]
    sage: D = -12*omega - 23
    sage: [A1,B1,C1] = ABC[0]
    sage: assert all([B^2 - 4*A*C == D for [A,B,C] in ABC])
    sage: assert all([(B-B1) in K.ideal(3) for [A, B, C] in ABC])
    sage: assert all([K.ideal(C/3, 3) == K.ideal(1) for [A, B, C] in ABC])
    sage: z1 = (-x^3-34*x-11)/22
    sage: assert A1*z1^2+B1*z1+C1 == 0
    sage: z = [[(-B + s*sqrt(B^2-4*A*C))/2/A for s in [-1,1]] for [A,B,C] in ABC]
    sage: z_xi = [[(u,((u - K.complex_conjugation()(u))*lmb)^-1) for u in v] for v in z]
    sage: z_xi = [[(u,xi) for (u,xi) in v if Phi.is_totally_positive_imaginary(xi)] for v in z_xi]
    sage: assert all([len(v) == 1 for v in z_xi])
    sage: z_xi = [v[0] for v in z_xi]
    sage: assert z_xi[0][0] == z1
    sage: z = [u for (u,xi) in z_xi]
    sage: tau = [period_matrix_from_z(zi, [omega, 1], lmb) for zi in z]
    sage: tau[0]
    Period Matrix
    [0.50000000000000? + 4.1498183124610?*I 0.50000000000000? + 1.8108031294328?*I]
    [0.50000000000000? + 1.8108031294328?*I           0.?e-15 + 2.3390151830282?*I]

This is exactly the period matrix printed in the article.
Just to be sure, Corollary 3.3 gives::

    sage: assert omega == (1+lmb)/2
    sage: z1 = phi1(z[0])
    sage: z2 = phi2(z[0])
    sage: om1 = phi1(omega)
    sage: om2 = phi2(omega)
    sage: l1 = phi1(lmb)
    sage: tau33 = 1/(-l1)*Matrix([[z1*om1^2-z2*om2^2, z1*om1-z2*om2],[z1*om1-z2*om2, z1-z2]])
    sage: tau33 - tau[0].complex_matrix()
    [0.?e-14 + 0.?e-13*I 0.?e-14 + 0.?e-13*I]
    [0.?e-14 + 0.?e-13*I 0.?e-15 + 0.?e-13*I]

So Corollary 3.3 agrees. We also check it against Proposition 3.2::

    sage: b11 = omega
    sage: b12 = 1
    sage: b21 = -1
    sage: b22 = 1-omega
    sage: B = [z[0]*b11, z[0]*b12, b21, b22]
    sage: Matrix([[(z_xi[0][1]*K.complex_conjugation()(u)*v).trace() for v in B] for u in B])
    [ 0  0  1  0]
    [ 0  0  0  1]
    [-1  0  0  0]
    [ 0 -1  0  0]
    sage: Om1 = Matrix([[phi(b) for b in B[:2]] for phi in [phi1,phi2]])
    sage: Om2 = Matrix([[CC(phi(b)) for b in B[2:]] for phi in [phi1,phi2]])
    sage: Om2.inverse()*Om1 - tau[0].complex_matrix()
    [ 2.77555756156289e-16 + 1.77635683940025e-15*I  5.55111512312578e-17 + 8.88178419700125e-16*I]
    [-5.55111512312578e-17 - 2.22044604925031e-16*I                         4.44089209850063e-16*I]

Again agrees. So let us continue::

    sage: prec = 400
    sage: f = [evaluate_single_I4_quotient(t, 3, prec) for t in tau]
    sage: f[0]
    4.31041770567796242256320321684273203414147817279906570986764136020750183037982085441439762155817057120652993884078975351 - 1.05769871912283540433297720532035616536433263820214215744873731082101815679111938896084577257613688382236375069816988466*I
    sage: X = polygen(f[0].parent())
    sage: F = prod([X-v for v in f]); F
    x^3 + (-1520.81864577885822782321793422981577187889860670222460777101804289432171781912911849441065430555419227726635898904798221 + 358.629756234205144714067111407968340020427057721666462575505830834803674150215333146459676191445410355422921500424834105*I)*x^2 + (120340.426264405965468052107205634518826205113315685693429563532850861857872634873213330732600292215264186042356814935327 - 39203.2377567834013587591648288507951969965823611375197805848001697610987725027157391769730050007185813850392566436730543*I)*x - 454033.008835683648854405399204026551864063010517557523490876429804648542231314199234699720187672323248782933469341824483 + 276194.792435730065214642559953375647210912955910345172417872888288525468689036132367631865235187325208466487755183228530*I
    sage: Kr = Phi.reflex_field()
    sage: pol = recognize_polynomial(F, Kr)
    sage: pol.denominator().factor()
    2^3 * 11^5 * 31^2
    sage: pol*pol.denominator()
    1238160088*y^3 + (8560748430*alphar^3 + 11670666480*alphar^2 + 970800040530*alphar - 617685149664)*y^2 + (401850769605*alphar^3 - 3039243175155*alphar^2 + 38906895998175*alphar - 180513547604841)*y - 2982488461975*alphar^3 + 4298737055525*alphar^2 - 290518295198065*alphar - 96097164139933

We double-check that this agrees with what we printed in the paper. The following
polynomial is copied from the tex file::

    sage: t = Kr.gen()
    sage: Kr.embedding()(t)
    10.41248483930372?*I
    sage: X = polygen(Kr, 'y')
    sage: dprime = 2^3*11*11^4*31^2
    sage: Fprime = 2^3 * 11^5 * 31^2 * X^3 + (8560748430 *t^3 + 11670666480 *t^2+ 970800040530 *t - 617685149664) *X^2  + (401850769605 *t^3 - 3039243175155 *t^2 + 38906895998175 *t - 180513547604841)* X  + (- 2982488461975 *t^3 + 4298737055525 *t^2- 290518295198065* t - 96097164139933)
    sage: Fprime == dprime * pol
    True

Next, the square roots::

    sage: fsqrt = [sqrt(a) for a in f]
    sage: fsqrt = [a if a.imag() < 0 else -a for a in fsqrt]

By trial-and-error, we choose the signs::

    sage: fsqrt[2] = -fsqrt[2]
    sage: [sign(v.real()) for v in fsqrt]
    [1, 1, -1]
    sage: F = prod([X-v for v in fsqrt]); F
    y^3 + (-31.2666429224671195659380591861447529421964406816119134762146796078045485490853486628831730026924808489776273749762664973 + 4.57396781276386932440989857740330151613532467628873431023231139328635820552977134701406153009161412982042394407517232957*I)*y^2 + (-282.068433844997451574151746387191394594615521371942845291030605550774277228622580911336615818247982758330399849055436413 + 36.3022597765567264283269659116505968490626537623134903208438092233996576305321727639286212969639832775783417994093896262*I)*y + 701.952280727003425288873425427337248262995251690121791481535219724613284052005623459287017896107850791597201373996762171 - 196.733310809730886236634742414542836555114618785633972022056936089630196500345831476824177284235732165600419466902097151*I
    sage: pol = recognize_polynomial(F, Kr)
    sage: pol.denominator().factor()
    2^3 * 11^3 * 31
    sage: pol*pol.denominator() == 2^3 * 11^3 * 31 * X^3 + (44850*t^3-26268*t^2+5007630*t- 13168716)*X^2 + (-639765*t^3+657855*t^2-68212395*t-21782871)*X+(693935*t^3-453871*t^2+68999645*t+182497403)
    True

Finally we check that the printed irreducible gusa class polynomial factor
corresponds to the N-system::

    sage: I4 = igusa_modular_forms()[0]
    sage: I6 = igusa_modular_forms()[1]
    sage: I10 = igusa_modular_forms()[2]
    sage: prec = 200
    sage: j = [I4(t, prec=prec, interval=True) * I6(t, prec=prec, interval=True) / I10(t, prec=prec, interval=True) for t in tau]
    sage: P = PolynomialRing(j[0].parent(), 'x')
    sage: for (p,e) in K0r['x'](icp[0]).factor():
    ....:     print(p*p.denominator(), [0 in P(p)(t) for t in j])
    841*x^3 + (-5611098752*omegar - 17741044214880)*x^2 + (3232391784287232*omegar - 68899837678801920)*x + 7331944332391841792*omegar - 131969791422849515520 [True, True, True]
    841*x^3 + (5611098752*omegar - 17746655313632)*x^2 + (-3232391784287232*omegar - 65667445894514688)*x - 7331944332391841792*omegar - 124637847090457673728 [False, False, False]

So the correct one is the one printed in the paper. As a sanity check
we evaluate the Igusa invariants in another way and compare the results::

    sage: j2 = [evaluate_mod_form(I4, t, prec)*evaluate_mod_form(I6, t, prec)/evaluate_mod_form(I10, t, prec) for t in tau]
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    Warning: only works for modular forms for level 1, please check if level 1
    sage: [j[i].real().upper() - j2[i].real() for i in range(3)] # check that they are close
    [2.2198914576141740158584839681573347892687499207400492465793e-46,
     1.6953684118271557132141345193043989035100534676359849121399e-53,
     2.7429219289114110324972451136605354783945401789484433211106e-53]
    sage: [j[i].real().lower() - j2[i].real() for i in range(3)] # check that they are close
    [-2.0439166291017038545165910471600812223675573581539328403055e-46,
     -1.7248724740213287293847093842710569261496007239981522120580e-53,
     -2.7629948869916799311878090066724086471449880963438703178152e-53]


"""
