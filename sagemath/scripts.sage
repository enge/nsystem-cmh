"""
This file contains helper functions for the SageMath version
of some of the examples in Section 7 of "Schertz style class invariants
for higher degree CM fields" by Enge and Streng.

It was tested with SageMath 10.2 and Recip 3.4.1, available at
https://www.sagemath.org
https://github.com/mstreng/recip
https://bitbucket.org/mstreng/recip/

To use this file, first update the location of Recip below and then do this::

    sage: load("scripts.sage")

Alternatively, if your SageMath installation is built with SSL, remove
the lines with load_attach_path and load("recip.sage") and do this::

    sage: load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage") # optional: ssl
    sage: load("scripts.sage")

To test this file automatically, use "sage -t scripts.sage" as well as e.g.
"sage -t example1.sage".


#*******************************************************************************
# Copyright (C) 2011 - 2024 Marco Streng
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/
#*******************************************************************************

"""

# UPDATE THE FOLLOWING LINE YOURSELF
load_attach_path("~/Documents/marco_werk/recip")
load("recip.sage")



def period_matrix_from_z(z, bas=None, lmb=None, ideal=None):
    """
    Given z (in a quartic CM-field K, not in the totally real field K_0)
    and a basis bas of O_{K_0},
    returns a period matrix corresponding to z*O_{K_0}+O_{K_0}
    
    EXAMPLE::
    
        sage: load("scripts.sage")
        sage: K = CM_Field([5,58,661])
        sage: alpha = K.gen()
        sage: z = (alpha-1)/2
        sage: w = (z^2+z+6)/-3
        sage: Z = period_matrix_from_z(z, [1,w], 2*w-1, K.ideal(1))
        sage: Z.basis() == [ z,z*w,1-w,K(-1)]
        True
        sage: [phi(Z.xi()) for phi in Z.CM_type()]
        [0.068666996952682?*I, 0.113287296096620?*I]
        sage: Z
        Period Matrix
        [          0.?e-15 + 2.33901518302821?*I  0.50000000000000? + 1.8108031294328?*I]
        [0.50000000000000? + 1.81080312943277?*I  0.50000000000000? + 4.1498183124610?*I]

    """
    K = z.parent()
    K0 = K.real_field()
    c = K.complex_conjugation()
    if bas is None:
        bas = z.parent().real_field().maximal_order().basis()
        bas = [K.real_to_self(x) for x in bas]
    bas = [K(x) for x in bas]

    if lmb is None:
        print("Warning, results may be unpredictable if the fixed square root lambda of Delta_0 is not given")
        delta = (bas[0]*bas[1].trace() - bas[1]*bas[0].trace())/2
    else:
        delta = lmb
        delta = K(delta)
        assert ((bas[0]*bas[1].trace() - bas[1]*bas[0].trace())/2) / delta in [1, -1]

    for x in bas:
        if not c(x) == x:
            raise ValueError("basis not real")
        if not x.norm() in ZZ or not x.trace() in ZZ:
            raise ValueError("basis not integral")


    if not delta^2 == K.DAB()[0]:
        print(delta^2)
        print(K.DAB())
        raise ValueError("incorrect discriminant")

    dual_bas = trace_dual_basis(bas)
    dual_bas = [-delta*b for b in dual_bas]

    assert Matrix([[((x*y)/-delta).trace()/2 for x in bas] for y in dual_bas]) == identity_matrix(2)

    xi = (z-c(z))^-1 * delta^-1

    Phi = CM_Type(xi)
    basis = [z*bas[0], z*bas[1], dual_bas[0], dual_bas[1]]
    return PeriodMatrix_CM(Phi, ideal, xi, basis)

@cached_function
def pre_mod_form_g():
    return ThetaProduct([0,0,0,0])*ThetaProduct([0,0,1/2,0])*ThetaProduct([0,0,0,1/2])*ThetaProduct([0,0,1/2,1/2])

def eval_mod_form_g(tau):
    return pre_mod_form_g()(tau/2)

@cached_function
def mod_form_f():
    return ThetaProduct([0,0,0,0])*ThetaProduct([1/2,0,0,0])*ThetaProduct([0,1/2,0,0])*ThetaProduct([1/2,1/2,0,0])

def eval_mod_form_f(tau):
    return mod_form_f()(tau)

def trace_dual_basis(bas, F=None):
    """
    Returns the trace dual basis of bas for tr_{F/Q}.
    
    Assumes F has degree 2.
    """
    if F is None:
        F = (bas[0]+bas[1]).parent()
    M = Matrix([[F(x*y).trace()/2 for x in bas] for y in bas])
    return [(u[0]*bas[0]+u[1]*bas[1]) for u in M.inverse()]


def z_from_ABC(A,B,C,K=None,Phi=None):
    """
    Returns a root of Ax^2+Bx+C. In K if K is provided.
    """
    x = polygen((A+B+C).parent())
    if K is None:
        rts = (A*x^2+B*x+C).roots()
        if len(rts) == 0:
            raise NotImplementedError()
    else:
        rts = (A*x^2+B*x+C).roots(K)
    z = rts[0][0]
    if not Phi is None:
        raise NotImplementedError()
    return z

def period_matrix_from_ABC(A,B,C,K=None,Phi=None,bas=None, ideal=None):
    z = z_from_ABC(A,B,C,K,Phi)
    return period_matrix_from_z(z, bas=bas, ideal=ideal)



def ideal_to_OKzero_basis(aaa):
    """
    Given an ideal aaa of K, returns an O_{K_0}-basis.
    """
    raise NotImplementedError()
    # Start by intersecting with K0
    K = aaa.number_field()
    K0 = K.real_field()
    phi = K.real_to_self()

def pre_list_pp_ideal_classes(K):
    """
    Assumes K is given by [D,A,B]
    
    Partial implementation of Algorithm A.2.5 of my thesis.
    """
    K0 = K.real_field()
    if K0.degree() != 2:
        raise NotImplementedError()
    if K0.class_number() > 1:
        raise NotImplementedError()
    S = [K0.ideal(1)]
    OK = K.maximal_order()
    bas = OK.basis()
    D0 = K0.discriminant()
    tot_pos_units_mod_squares = [1]
    eps = K0(K0.unit_group().gens()[1])
    epsnorm = eps.norm()
    OKzero = K0.maximal_order()
    if epsnorm == 1:
        tot_pos_units_mod_squares = [1,eps]
    phi = K.real_to_self()
    sqrtD = K.gen()
    [D,A,B] = K.DAB()
    D = (K0(A^2-4*B).sqrt() - A)/2
    if phi(D) != sqrtD^2:
        D = -A-D
    assert phi(D) == sqrtD^2
    # Start of step 3
    for b in S:
        a_over_bsqr_ideals = flatten(K0.ideals_of_bdd_norm(sqrt(D.norm())*D0*6/pi^2).values())
        for a_over_bsqr_ideal in a_over_bsqr_ideals:
            a_ideal = a_over_bsqr_ideal * b^2
            if a_ideal.is_principal():
                g = a_ideal.gens_reduced()[0]
                if epsnorm == -1:
                    if g.norm() < 0:
                        g = g*epsnorm
                if g.norm() > 0:
                    if not g.is_totally_positive():
                        g = -g
                    for e in tot_pos_units_mod_squares:
                        a = g*e
                        # Start of steps 4 and 5
                        bas = a_over_bsqr_ideal.basis()
                        n = a_over_bsqr_ideal.gens_two()[0]
                        assert n in ZZ
                        for x in range(n):
                            for y in range(n):
                                b_elt = x/n*bas[0] + y/n*bas[1]
                                if b_elt in OKzero and (b_elt^2-D)/a in OKzero:
                                    # Start of step 6
                                    # actually, instead do this:
                                    z = (sqrtD - phi(b_elt)) / phi(a)
                                    if is_valid_z(z):
                                        yield z

def is_valid_z(z):
    """
    Returns true if and only if z*OKzero + OKzero has endomorphism ring OK.
    """
    K = z.parent()
    bar = K.complex_conjugation()
    OK = K.maximal_order()
    a = z*OK + 1*OK
    return a.norm()^2 == (z-bar(z)).norm()/K.discriminant()*K.real_field().discriminant()^2

def z_to_ideal_and_xi(z, delta):
    K = z.parent()
    bar = K.complex_conjugation()
    OK = K.maximal_order()
    a = z*OK + 1*OK
    xi = (z-bar(z))^-1 * delta^-1
    assert a*bar(a)*K.different()*xi == 1
    return a, xi

def are_equivalent(a1,xi1,a2,xi2):
    K = xi1.parent()
    bar = K.complex_conjugation()
    mu_ideal = a1/a2
    if not mu_ideal.is_principal():
        return False
    m = mu_ideal.gens_reduced()[0]
    # mu = m * u for a unit u
    # m*mbar * u*ubar = mu*mubar = xi2 / xi1
    u_times_ubar = xi2/xi1/m/bar(m)
    if u_times_ubar == 1:
        return True
    print(K.ideal(u_times_ubar))
    print(u_times_ubar)
    print(u_times_ubar.parent())
    
    # return true if and only if there is a u with u*bar(u) = u_times_ubar
    ug = K.unit_group()
    ug(u_times_ubar)
    gens_of_norms = [ug(K(g)*bar(K(g))) for g in ug.gens()]
    return u_times_ubar in ug.subgroup(gens_of_norms)


def list_pp_ideal_classes_using_z(K, delta):
    l = []
    for z in pre_list_pp_ideal_classes(K):
        a, xi = z_to_ideal_and_xi(z, delta)
        for (z1, a1, xi1) in l:
            if are_equivalent(a, xi, a1, xi1):
                print("old")
                break
        else:
            print("new")
            l.append((z, a, xi))
    return l

def z_to_ABC(z):
    A = 1
    K = z.parent()
    bar = K.complex_conjugation()
    B = - (z+bar(z))
    C = z*bar(z)
    den = lcm(B.denominator(), C.denominator())
    return (den, den*B, den*C)

def is_equivalent_to_element(a, xi, lst):
    for (a1, xi1) in lst:
        if are_equivalent(a, xi, a1, xi1):
            return True
    return False

def K_to_K0(A):
    K = A.parent()
    try:
        phi = K.relative_field().structure()[1]
    except AttributeError:
        return QQ(A)
    A = phi(A)
    assert A[1] == 0
    return A[0]


def ABC_to_K0(ABC):
    """
    From ABC in K, give ABC in K0
    """
    return tuple(K_to_K0(A) for A in ABC)

def ABC_coprime(ABC):
    (A,B,C) = ABC
    K0 = (A+B+C).parent()
    OK0 = K0.maximal_order()
    d = A*OK0 + B*OK0 + C*OK0
    if not d.is_principal():
        raise NotImplementedError()
    d = d.gens_reduced()[0]
    return (A/d, B/d, C/d)

def ABC_for_N(ABC, N):
    """
    Replaces ABC by an equivalent triple that is good for our class invariants.
    """
    (A,B,C) = ABC
    K0 = (A+B+C).parent()
    OK0 = K0.maximal_order()
    if N*OK0 + A*OK0 != 1:
        if N*OK0 + A*OK0 != 1:
            x = 1
            (A,B,C) = (C*x^2 + B*x + A, B + 2*C*x, C)
        else:
            (A,B,C) = (C,-B,A)
    assert N*OK0 + A*OK0 == 1
    for x in (N*OK0).residues():
        # A' X^2 + B' X + C' = A(X+x)^2 + B(X+x) + C and N|C'
        # then A' = A
        # B' = B + 2*A*x
        # C' = A*x^2 + B*x + C
        if A*x^2 + B*x + C in N*OK0:
            return (A, B+2*A*x, A*x^2+B*x+C)
    raise ValueError("are you sure N is split (or ramified and not a power)?")

def ABC_to_z(ABC, K):
    phi = K.real_to_self()
    x = K['x'].gen()
    A,B,C = ABC
    zs = (phi(A)*x^2+phi(B)*x+phi(C)).roots()
    # TODO: make sensible choice
    return zs[0][0]

def scale_ABC(ABC,x):
    A,B,C = ABC
    return (x*A,x*B,x*C)


def evaluate_I4(Z, prec):
    (U, M) = Z.reduce(200, transformation=True)
    I4 = igusa_modular_forms()[0]
    A,B,C,D = ABCD(M)
    return (C * Z.complex_matrix(prec) + D).determinant()^-4 * I4(U, prec=prec)

def evaluate_Theta(Z, prec, reduce=True):
    """
    Evaluates the square root of I10 given by the product of the ten
    even theta constants.
    """
    if not reduce:
        return Theta(Z, prec=prec)
    
    # theta[d](0, M(Z)) = kappa(M)*det(C*Z+D)^(1/2)*e(s)*theta[c](0, Z)
    # for c, s, as in theta_action_without_kappa(M, 1, d)
    #
    # We take all even theta's for c, hence for d:
    #
    # Theta(M(Z)) = kappa(M)^10 det(CZ+D)^5 e(sum_d(s)) Theta(Z)
    # U = M(Z), S = sum_d(s)



    (U, M) = _reduce(Matrix(ComplexField(prec), Z))
    try:
        U = Matrix(ComplexField(prec), Sp_action(M, Z))
    except ZeroDivisionError:
        U = Sp_action(M, Matrix(ComplexField(prec), Z))
    #.reduce(200, transformation=True)
    S = sum([theta_action_without_kappa(M, 1, d)[1] for d in even_theta_characteristics()])
    A,B,C,D = ABCD(M)
    f1 = determine_kappa(M)^-2
    f2 =(C*Z+D).determinant()^-5
    f3 = ComplexField(prec)(exp(2*pi*I*-S))
    f4 = Theta(U)
    return f1 * f2 * f3 * f4

th = theta_ring(2,2)[0].gens()
Theta = ThetaModForm(prod([th[i] for i in [0,1,2,3,4,6,8,9,12,15]]))

def period_matrix_div(Z, N):
    basZ = Z.basis()
    g = len(basZ)/2
    bas = [b/N for b in basZ[:g]] + basZ[g:]
    return PeriodMatrix_CM(CM_type=Z.CM_type(), xi=N*Z.xi(), basis=bas)


def evaluate_double_I4_quotient(Z, p, q, prec):
    return evaluate_I4(period_matrix_div(Z, p), prec=prec) * evaluate_I4(period_matrix_div(Z, q), prec=prec) / evaluate_I4(period_matrix_div(Z, p*q), prec=prec) / evaluate_I4(Z, prec=prec)

def evaluate_single_I4_quotient(Z, p, prec):
    return evaluate_I4(period_matrix_div(Z, p), prec=prec) / evaluate_I4(Z, prec=prec)

def evaluate_single_Theta_quotient(Z, p, prec, reduce=True):
    return evaluate_Theta(period_matrix_div(Z, p), prec=prec, reduce=reduce) / evaluate_Theta(Z, prec=prec, reduce=reduce)

gamma_02gens = [(ThetaModForm([0,0,0,0])^4+ThetaModForm([0,0,0,1/2])^4+ThetaModForm([0,0,1/2,0])^4+ThetaModForm([0,0,1/2,1/2])^4),
(ThetaModForm([0,0,0,0])*ThetaModForm([0,0,0,1/2])*ThetaModForm([0,0,1/2,0])*ThetaModForm([0,0,1/2,1/2]))^2]

AIx = ThetaModForm(sum([t^4 for t in th[:4]])/4) # X of Aoki-Ibukiyama
AIysqrt = ThetaModForm(prod(th[:4])) # the square of this is Y of Aoki-Ibukiyama
AIz = ThetaModForm((th[8]^4-th[9]^4)^2/16384) # Z of Aoki-Ibukiyama
AIk = ThetaModForm((th[8]*th[9]*th[4]*th[6]*th[12]*th[15])^2/4096)  # K of Aoki-Ibukiyama


def evaluate_mod_form(mf, Z, prec):
    """
    Warning: only works for modular forms for level 1
    """
    print("Warning: only works for modular forms for level 1, please check if level 1")
    (U, M) = Z.reduce(200, transformation=True)
    A,B,C,D = ABCD(M)
    k = mf.weight()
    return (C * Z.complex_matrix(prec) + D).determinant()^-k * mf(U, prec=prec)




def invert_2by2_matrix(M):
    return 1/M.determinant() * Matrix([[M[1,1], -M[0,1]],[-M[1,0], M[0,0]]])


