\r common.gp

A = 53;
B = 601;
N = 6;
cm = init_cmfield (A, B);

g = tau -> double_quotient (I10, tau, 2, 3);

printf ("\nCLASS INVARIANT\n");
f = classpol_complex (cm, N, g, 1, 0, 1);
den = 2^4 * 13^4;
H = guess_classpol_real (cm, real (f), den);

default ("realprecision", 100);
printf ("\nSTRENG INVARIANT\n");
f = classpol_complex (cm, 1, J1, 1, 0);
den = 2^40 * 13^4;
H = guess_classpol_real (cm, real (f), den);

