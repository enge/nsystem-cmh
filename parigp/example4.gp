\r common.gp

A = 11;
B = 29;
N = 14;
cm = init_cmfield (A, B);
default (realprecision, 100);

g = tau -> { my (h); h = simple_quotient (I4, tau, N); h + N^8 / h;}

printf ("\nCLASS INVARIANT\n");
/* Copy-paste code from classpol_complex in common.gp to work with
   a particular nsystem. */
nsystem = [[1, [-2, 12]~, [42, 28]~]];
basis = symplectic_basis (cm, nsystem [1]);
Msym = period_matrix (cm, basis);
printf ("period matrix (symbolic):\n%Ps\n", Msym);
Kr = nfinit (cm [7].pol);
M = apply (x -> subst (x, y, Kr.roots [2]), Msym);
printf ("period matrix (complex):\n%Ps\n", M);
con = g (M);
printf ("root of class polynomial:\n%Ps\n", con);
con_Kr = algdep (con, 4, 100);
printf ("minimal polynomial of root:\n%Ps\n", con_Kr);

