\r common.gp

A = 18;
B = 68;
N = 2;
cm = init_cmfield (A, B);

evaluate (tau) =
{
   my (X, Y, Z, K, F);
   [X, Y, Z, K, F] = ibukiyama (tau);
   return (X^2 / Y);
}

g = tau -> evaluate (tau);

default ("realprecision", 40);
printf ("\nCLASS INVARIANT\n");
f = classpol_complex (cm, N, g, 1, 0);
den = 19^4 * 59^2;
H = guess_classpol_real (cm, real (f), den);

default ("realprecision", 100);
printf ("\nSTRENG INVARIANT\n");
f = classpol_complex (cm, 1, J1, 1, 0);
den = 19^2 * 59^2;
H = guess_classpol_real (cm, real (f), den);

