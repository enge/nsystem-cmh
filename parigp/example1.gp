\r common.gp

A = 57;
B = 661;
N = 3;
cm = init_cmfield (A, B);
g = tau -> simple_quotient (I4, tau, N);

printf ("\nCLASS INVARIANT\n");
f = classpol_complex (cm, N, g, 1, 1);
den = 2^3 * 11^5 * 31^2;
H = guess_classpol (cm, f, den);

default ("realprecision", 100);
printf ("\nSTRENG INVARIANT\n");
f = classpol_complex (cm, 1, J1, 1, 0);
den = 29^2;
H = guess_classpol_real (cm, real (f), den);

