# nsystem-cmh #

This project contains software accompanying the article
[*Schertz style class invariants for quartic CM
fields*](https://hal.science/hal-01377376/)
by Andreas Enge and Marco Streng.

### Pari-GP ###

The subdirectory `parigp` contains GP scripts for computing the examples
of the article; these rely on
[PariTwine](https://www.multiprecision.org/paritwine/)
for integrating additional libraries into PARI/GP, and ultimately on
[CMH](https://www.multiprecision.org/cmh/)
for computing 2-dimensional theta functions.

They can be run reproducibly with all code dependencies at the versions
at the time when the article was finalised using
[GNU Guix](https://guix.gnu.org/) as follows:
```
guix time-machine -C channels.scm -- shell -C -m manifest.scm -- gp < example1.gp
```
and analogously for the other examples.

### SageMath ###

The subdirectory `sagemath` contains [SageMath](https://sagemath.org/)
scripts for computing the examples of the article; these rely on
[Recip](https://github.com/mstreng/recip/).

Each example can be run using the instructions at the top of the
respective file.

